import os

import environ

ENV = environ.Env()
environ.Env.read_env(".env")
BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))


SECRET_KEY = ENV("SECRET_KEY", default="hell no")
DEBUG = ENV.bool("DEBUG", default=False)
ALLOWED_HOSTS = ENV.list("ALLOWED_HOSTS", default=["localhost", "127.0.0.1"])
ROOT_URLCONF = "src.urls"

# Application definition

INSTALLED_APPS = [
    "django.contrib.admin",
    "django.contrib.auth",
    "django.contrib.contenttypes",
    "django.contrib.sessions",
    "django.contrib.messages",
    "django.contrib.staticfiles",
    # 3rd party
    "rest_framework",
    "django_extensions",
    "django_celery_beat",
    # local
    "src.company",
]

MIDDLEWARE = [
    "django.middleware.security.SecurityMiddleware",
    "django.contrib.sessions.middleware.SessionMiddleware",
    "django.middleware.common.CommonMiddleware",
    "django.middleware.csrf.CsrfViewMiddleware",
    "django.contrib.auth.middleware.AuthenticationMiddleware",
    "django.contrib.messages.middleware.MessageMiddleware",
    "django.middleware.clickjacking.XFrameOptionsMiddleware",
]


if DEBUG:
    ALLOWED_HOSTS = ["*"]
    INSTALLED_APPS = INSTALLED_APPS + ["corsheaders"]
    MIDDLEWARE = [
        "corsheaders.middleware.CorsMiddleware",
        "querycount.middleware.QueryCountMiddleware",
    ] + MIDDLEWARE
    CORS_ORIGIN_ALLOW_ALL = True
    QUERYCOUNT = {
        "THRESHOLDS": {
            "MEDIUM": 50,
            "HIGH": 200,
            "MIN_TIME_TO_LOG": 0,
            "MIN_QUERY_COUNT_TO_LOG": 0,
        },
        "IGNORE_REQUEST_PATTERNS": [r"^/admin/"],
        "IGNORE_SQL_PATTERNS": [],
        "DISPLAY_DUPLICATES": None,
        "RESPONSE_HEADER": "X-DjangoQueryCount-Count",
    }

TEMPLATES = [
    {
        "BACKEND": "django.template.backends.django.DjangoTemplates",
        "DIRS": [],
        "APP_DIRS": True,
        "OPTIONS": {
            "context_processors": [
                "django.template.context_processors.debug",
                "django.template.context_processors.request",
                "django.contrib.auth.context_processors.auth",
                "django.contrib.messages.context_processors.messages",
            ],
        },
    },
]

WSGI_APPLICATION = "src.wsgi.application"


# Password validation
# https://docs.djangoproject.com/en/3.0/ref/settings/#auth-password-validators

AUTH_PASSWORD_VALIDATORS = [
    {
        "NAME": "django.contrib.auth.password_validation.UserAttributeSimilarityValidator",
    },
    {
        "NAME": "django.contrib.auth.password_validation.MinimumLengthValidator",
    },
    {
        "NAME": "django.contrib.auth.password_validation.CommonPasswordValidator",
    },
    {
        "NAME": "django.contrib.auth.password_validation.NumericPasswordValidator",
    },
]


# Internationalization
# https://docs.djangoproject.com/en/3.0/topics/i18n/

LANGUAGE_CODE = "en-us"

TIME_ZONE = "UTC"

USE_I18N = True

USE_L10N = True

USE_TZ = True


# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/3.0/howto/static-files/

STATIC_URL = "/static/"


"""
Database
"""
DATABASES = {
    "default": ENV.db_url(
        "DATABASE_URL",
        default="postgres://root:root@localhost:5433/sunscrapers_db?conn_max_age=0",
    )
}


"""
Celery
"""
CELERY_BROKER_URL = ENV(
    "CELERY_BROKER_URL", default="redis://localhost:6380/1"
)
CELERY_TASK_ALWAYS_EAGER = not CELERY_BROKER_URL
CELERY_ACCEPT_CONTENT = ["json"]
CELERY_TASK_SERIALIZER = "json"
CELERY_RESULT_SERIALIZER = "json"
CELERY_TIMEZONE = TIME_ZONE

CELERYBEAT_SCHEDULER = "djcelery.schedulers.DatabaseScheduler"


"""
Custom
"""
ALPHAVANTAGE_API_KEY = ENV("ALPHAVANTAGE_API_KEY")
ALPHAVANTAGE_BASE_URL = ENV(
    "ALPHAVANTAGE_BASE_URL", default="https://www.alphavantage.co/"
)
ALPHAVANTAGE_NAME_DIFF_COUTOFF = ENV(
    "ALPHAVANTAGE_NAME_DIFF_COUTOFF", default=0.75
)

CLEARBIT_BASE_URL = ENV(
    "CLEARBIT_BASE_URL", default="https://autocomplete.clearbit.com/v1/"
)

DAILY_QUOTE_TASK_NAME = "{symbol} daily quote"
