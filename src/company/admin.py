from django.contrib import admin

from . import models


class CompanyQuoteInline(admin.TabularInline):
    model = models.CompanyQuote
    extra = 0
    readonly_fields = ("date",)


@admin.register(models.Company)
class CompanyAdmin(admin.ModelAdmin):
    inlines = (CompanyQuoteInline,)
