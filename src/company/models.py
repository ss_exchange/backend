from django.db import models
from django.db.models.signals import post_delete
from django.dispatch import receiver
from django_celery_beat.models import PeriodicTask


class Company(models.Model):
    symbol = models.CharField(max_length=20, unique=True)
    tracking_task = models.OneToOneField(
        PeriodicTask, on_delete=models.CASCADE, blank=True, null=True
    )

    def __str__(self):
        return self.symbol


@receiver(post_delete, sender=Company)
def my_post_delete_callback(sender, instance, **kwargs):
    task = instance.tracking_task
    if task:
        task.delete()


class CompanyQuote(models.Model):
    date = models.DateField(auto_now_add=True)
    open = models.FloatField()
    high = models.FloatField()
    low = models.FloatField()
    close = models.FloatField()
    volume = models.IntegerField()
    company = models.ForeignKey(
        Company,
        related_name="quotes",
        related_query_name="quote",
        on_delete=models.CASCADE,
    )

    class Meta:
        unique_together = ("date", "company")

    def __str__(self):
        return f"{self.company.symbol}: {self.date}"
