import logging
from datetime import datetime

from django.db import IntegrityError
from django.utils import timezone
from rest_framework.exceptions import APIException

from ..celery import app
from ..providers import alpha_vantage_provider
from .models import Company, CompanyQuote

logger = logging.getLogger(__name__)


@app.task
def store_comapny_quote_daily(symbol: str):
    try:
        data = alpha_vantage_provider.company_last_trading_day(symbol)
    except APIException:
        logger.exception(
            "Failed to fetch company quote", extra={"symbol": symbol,},
        )
    else:
        date = datetime.strptime(data["date"], "%Y-%m-%d").date()
        today_date = timezone.now().date()

        if date != today_date:
            logger.info("Latest date isn't todays")
            return

        data.pop("date")
        company, _ = Company.objects.get_or_create(symbol=symbol)

        try:
            CompanyQuote.objects.create(company=company, **data)
        except IntegrityError:
            logger.exception(
                "Failed to create CompanyQuote for Company %s",
                company.id,
                extra={"symbol": symbol,},
            )
