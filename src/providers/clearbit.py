import difflib
import re
from typing import Dict, List, Union

from django.conf import settings
from rest_framework.exceptions import NotFound

from .base import BaseProvider
from .exceptions import CompanyNotFound

# captures all company suffixex ending with dot like Inc., Ltd. etc
COMPANY_SUFFIX_PATTERN = re.compile(r"\w+(?:\?|\.|gy\b)")


class ClearbitProvider(BaseProvider):
    @staticmethod
    def find_best_matched_company_by_name(
        sequence: List[Dict[str, str]], name: str
    ) -> Union[Dict[str, str], None]:
        names = [i["name"] for i in sequence]
        match = difflib.get_close_matches(
            name, names, n=1, cutoff=settings.ALPHAVANTAGE_NAME_DIFF_COUTOFF,
        )
        return (
            next((i for i in sequence if i["name"] == match[0]), None)
            if match
            else None
        )

    def _call(self, *args, **kwargs):
        response = super()._call(*args, **kwargs)
        json = response.json()
        return json

    def logo_details(self, name: str) -> Dict[str, str]:
        """
        Example response:
        {
            "name": "Alphabet",
            "domain": "abc.xyz",
            "logo": "https://logo.clearbit.com/abc.xyz"
        }
        """
        cleaned_name = COMPANY_SUFFIX_PATTERN.sub("", name).strip()
        results = self._call(
            params={"query": cleaned_name}, endpoint="companies/suggest?"
        )
        company = self.find_best_matched_company_by_name(results, cleaned_name)

        if not results and not company:
            raise CompanyNotFound()
        return company
