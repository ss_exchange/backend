from rest_framework.exceptions import NotFound, ParseError, Throttled


class CompanyNotFound(NotFound):
    pass


class CompanyRequestsExceded(Throttled):
    pass


class ComapnyParseError(ParseError):
    pass
