from django.conf import settings

from .alpha_vantage import AlphaVantageProvider
from .clearbit import ClearbitProvider

clearbit_provider = ClearbitProvider(base_url=settings.CLEARBIT_BASE_URL)

alpha_vantage_provider = AlphaVantageProvider(
    base_url=settings.ALPHAVANTAGE_BASE_URL,
    api_key=settings.ALPHAVANTAGE_API_KEY,
)
