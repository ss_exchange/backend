import logging
from urllib.parse import urljoin

import requests

logger = logging.getLogger(__name__)


class BaseProvider:
    def __init__(self, base_url, api_key=None, verify=True):
        self.BASE_URL = base_url
        self.API_KEY = api_key
        self.verify = verify

    def _make_url(self, endpoint):
        """Constructs a full url from an endpoint."""
        return urljoin(self.BASE_URL, endpoint)

    def _call(self, endpoint, method=requests.get, **kwargs):
        """Does an actual call to the service."""
        url = self._make_url(endpoint)
        response = method(url, verify=self.verify, **kwargs)
        logger.debug(
            "%s : %s call to %s responded with %s status code. \n %s",
            self.__class__.__name__,
            method.__name__,
            response.request.url,
            str(response.status_code),
            response.content,
        )
        response.raise_for_status()
        return response
