import collections
import re
from typing import Dict, List

import requests
from django.http import Http404

from .base import BaseProvider
from .exceptions import (
    ComapnyParseError,
    CompanyNotFound,
    CompanyRequestsExceded,
)
from .utils import camel_case_to_underscore_recursive


class AlphaVantageProvider(BaseProvider):
    """ https://www.alphavantage.co/documentation """

    @staticmethod
    def parse_keys(data: dict):
        """
        Assumes all keys look like `1. symbol`, and takes the part after the space.
        Eg: `1. symbol` => `symbol`
        """
        keys = (key.split(" ")[1] for key in data.keys())
        values = data.values()
        return dict(zip(keys, values))

    def _call(self, function, method=requests.get, **kwargs):
        params = kwargs.pop("params", {})
        params["apikey"] = self.API_KEY
        response = super()._call(
            endpoint="query?",
            method=requests.get,
            params={**{"function": function}, **params},
            **kwargs
        )
        json = response.json()
        note = json.get("Note")
        error = json.get("Error Message")

        if note and "Our standard API call frequency" in note:
            raise CompanyRequestsExceded(detail=note)
        elif error:
            raise ComapnyParseError(detail=error)

        return json

    def search(self, symbol: str) -> Dict[str, str]:
        """
        Example response:
        [
                "symbol": "GOOG",
                "name": "Alphabet Inc.",
                "type": "Equity",
                "region": "United States",
                "market_open": "09:30",
                "market_close": "16:00",
                "timezone": "UTC-05",
                "currency": "USD",
                "match_score": "1.0000"
        ]
        """
        results = self._call(
            function="SYMBOL_SEARCH", params={"keywords": symbol},
        )

        if not results or not results.get("bestMatches"):
            raise CompanyNotFound()

        return camel_case_to_underscore_recursive(
            [self.parse_keys(i) for i in results["bestMatches"]]
        )

    def company(self, symbol: str) -> Dict[str, str]:
        results = self.search(symbol)
        if not results:
            raise CompanyNotFound()
        return results[0]

    def company_last_trading_day(self, symbol: str) -> Dict[str, str]:
        """
        Example response:
        {
            "date": "2020-02-07",
            "open": "182.8450",
            "high": "185.6300",
            "low": "182.4800",
            "close": "183.8900",
            "volume": "33529074"
        }
        """
        results = self._call(
            function="TIME_SERIES_DAILY", params={"symbol": symbol},
        )["Time Series (Daily)"]
        latest_entry = next(i for i in results)
        return {
            **self.parse_keys(results[latest_entry]),
            **{"date": latest_entry},
        }
