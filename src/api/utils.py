from django.utils.text import re_camel_case


def camel_case_to_underscore(value):
    return re_camel_case.sub(r"_\1", value).strip("_").lower()


def camel_case_to_underscore_recursive(obj):
    if isinstance(obj, dict):
        new_dict = {}
        for key, value in obj.items():
            if isinstance(key, str):
                key = camel_case_to_underscore(key)
            new_dict[key] = camel_case_to_underscore_recursive(value)
        return new_dict
    if isinstance(obj, list):
        return [camel_case_to_underscore_recursive(value) for value in obj]
    return obj
