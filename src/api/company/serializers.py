from django.conf import settings
from django.db import transaction
from django_celery_beat.models import CrontabSchedule, PeriodicTask
from rest_framework import serializers

from ...company import models, tasks
from ...providers import alpha_vantage_provider


class CompanySerializer(serializers.Serializer):
    symbol = serializers.SerializerMethodField()
    name = serializers.SerializerMethodField()
    type = serializers.SerializerMethodField()
    region = serializers.SerializerMethodField()
    market_open = serializers.SerializerMethodField()
    market_close = serializers.SerializerMethodField()
    timezone = serializers.SerializerMethodField()
    currency = serializers.SerializerMethodField()
    match_score = serializers.SerializerMethodField()

    def get_symbol(self, company):
        return company["symbol"]

    def get_name(self, company):
        return company["name"]

    def get_type(self, company):
        return company["type"]

    def get_region(self, company):
        return company["region"]

    def get_market_open(self, company):
        return company["market_open"]

    def get_market_close(self, company):
        return company["market_close"]

    def get_timezone(self, company):
        return company["timezone"]

    def get_currency(self, company):
        return company["currency"]

    def get_match_score(self, company):
        return company["match_score"]


class CompanyDetailSerializer(CompanySerializer):
    tracked = serializers.SerializerMethodField()

    def get_tracked(self, company):
        return company["tracked"]

    def save(self, **kwargs):
        import ipdb

        ipdb.set_trace()


class CompanyLogoSerializer(serializers.Serializer):
    logo = serializers.SerializerMethodField()
    domain = serializers.SerializerMethodField()

    def get_logo(self, company):
        return company["logo"]

    def get_domain(self, company):
        return company["domain"]


class CompanyLastTradingDaySerializer(serializers.Serializer):
    date = serializers.SerializerMethodField()
    open = serializers.SerializerMethodField()
    high = serializers.SerializerMethodField()
    low = serializers.SerializerMethodField()
    close = serializers.SerializerMethodField()
    volume = serializers.SerializerMethodField()

    def get_date(self, data):
        return data["date"]

    def get_open(self, data):
        return data["open"]

    def get_high(self, data):
        return data["high"]

    def get_low(self, data):
        return data["low"]

    def get_close(self, data):
        return data["close"]

    def get_volume(self, data):
        return data["volume"]


class CompanyQuoteSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.CompanyQuote
        exclude = ("company",)


class TrackedCompanySerializer(serializers.ModelSerializer):
    quotes = CompanyQuoteSerializer(many=True, read_only=True)

    class Meta:
        model = models.Company
        fields = ("id", "symbol", "quotes")

    @transaction.atomic
    def create(self, validated_data):
        symbol = validated_data["symbol"]

        # provider will raise 404 if not found
        alpha_vantage_provider.company(symbol)

        tasks.store_comapny_quote_daily.delay(symbol)

        schedule, _ = CrontabSchedule.objects.get_or_create(
            minute="0",
            hour="2",
            day_of_week="*",
            day_of_month="*",
            month_of_year="*",
        )
        task = PeriodicTask.objects.create(
            crontab=schedule,
            name=settings.DAILY_QUOTE_TASK_NAME.format(symbol=symbol),
            task="src.company.tasks.store_comapny_quote_daily",
        )
        return super().create({**validated_data, **{"tracking_task": task}})
