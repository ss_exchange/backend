from django.urls import path

from .views import (
    CompanyDetailsView,
    CompanyLastTradingDayTrackingListView,
    CompanyLastTradingDayTrackingView,
    CompanyLastTradingDayView,
    CompanyListView,
    CompanyLogoDetailsView
)

urlpatterns = [
    path("<str:symbol>/", CompanyDetailsView.as_view(), name="company_list"),
    path(
        "search/<str:symbol>/", CompanyListView.as_view(), name="company_list"
    ),
    path(
        "logo/<str:name>/",
        CompanyLogoDetailsView.as_view(),
        name="company_logo_detail",
    ),
    path(
        "last_trading_day/track/",
        CompanyLastTradingDayTrackingListView.as_view(),
        name="company_last_trading_day_list",
    ),
    path(
        "last_trading_day/<str:symbol>/",
        CompanyLastTradingDayView.as_view(),
        name="company_last_trading_day",
    ),
    path(
        "last_trading_day/track/<str:symbol>/",
        CompanyLastTradingDayTrackingView.as_view(),
        name="company_last_trading_day_track",
    ),
]
