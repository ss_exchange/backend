from django.conf import settings
from rest_framework import generics

from ...company import models
from ...providers import alpha_vantage_provider, clearbit_provider
from . import serializers


class CompanyListView(generics.ListAPIView):
    serializer_class = serializers.CompanySerializer

    def get_queryset(self):
        return alpha_vantage_provider.search(self.kwargs["symbol"])


class CompanyDetailsView(generics.RetrieveAPIView):
    serializer_class = serializers.CompanySerializer

    def get_object(self):
        return alpha_vantage_provider.company(self.kwargs["symbol"])


class CompanyLogoDetailsView(generics.RetrieveAPIView):
    serializer_class = serializers.CompanyLogoSerializer

    def get_object(self):
        return clearbit_provider.logo_details(self.kwargs["name"])


class CompanyLastTradingDayView(generics.RetrieveAPIView):
    serializer_class = serializers.CompanyLastTradingDaySerializer

    def get_object(self):
        return alpha_vantage_provider.company_last_trading_day(
            self.kwargs["symbol"]
        )


class CompanyLastTradingDayTrackingView(
    generics.RetrieveDestroyAPIView, generics.CreateAPIView
):
    queryset = models.Company.objects.all().prefetch_related("quotes")
    lookup_field = "symbol"
    serializer_class = serializers.TrackedCompanySerializer


class CompanyLastTradingDayTrackingListView(generics.ListAPIView):
    serializer_class = serializers.TrackedCompanySerializer
    queryset = models.Company.objects.all().prefetch_related("quotes")
