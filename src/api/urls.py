from django.urls import include, path

from .company.urls import urlpatterns as company_urls

urlpatterns = [
    path("company/", include(company_urls)),
]
