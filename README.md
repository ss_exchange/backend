# Backend

## Requirements

- python 3.7
- [Docker & Docker compose](https://docs.docker.com/compose/install/)
- [poetry](https://github.com/python-poetry/poetry)

### Development

    $ mkvirtualenv sunscrapers_back -p $(which python3.7) && setvirtualenvproject

    $ pip install poetry
    $ poetry install

    $ docker-compose build
    $ docker-compose up

    $ ./manage.py migrate
    $ ./manage.py runserver

Create `.env` file with needed variables

    $ touch .env

Sample `.env` file:

    ALPHAVANTAGE_API_KEY=your_key
    DEBUG=True
